`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.09.2015 16:54:00
// Design Name: 
// Module Name: dflipflop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module dflipflop(
    input clock,
    input reset,
    input D,
    output reg Q
    );
    
    always @(posedge clock or posedge reset)
    begin
        if (reset == 1'b1)
            Q <= 1'b0;
        else
            Q <= D;
    end
endmodule
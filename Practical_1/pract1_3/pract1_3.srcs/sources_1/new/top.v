`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.09.2015 17:18:18
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input in1,
    input in2,
    input in3,
    input c,
    input r,
    output sum,
    output cout
    );
    
    wire sumwire;
    wire carrywire;
    
    fulladder u_fulladder(
        .A      (in1),
        .B      (in2),
        .Cin    (in3),
        .S      (sumwire),
        .Cout   (carrywire)
    );
        
    dflipflop s_flipflop(
        .clock  (c),
        .reset  (r),
        .D      (sumwire),
        .Q      (sum)
    );
        
    dflipflop c_flipflop(
        .clock  (c),
        .reset  (r),
        .D      (carrywire),
        .Q      (cout)
    );
            
endmodule

#!/bin/sh -f
xv_path="/opt/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim tb_fa_behav -key {Behavioral:sim_1:Functional:tb_fa} -tclbatch tb_fa.tcl -log simulate.log

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.09.2015 16:33:50
// Design Name: 
// Module Name: tb_fa
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_fa;

reg     in1, in2, in3;
wire    out1, out2;
    
    fulladder u_fulladder (
        .A      (in1),
        .B      (in2),
        .Cin    (in3),
        .S      (out1),
        .Cout   (out2)
    );
    
initial
    begin
        {in1, in2, in3} = 3'b000;
        #100;
        #10 {in1, in2, in3} = 3'b001;
        #10 {in1, in2, in3} = 3'b010;
        #10 {in1, in2, in3} = 3'b011;
        #10 {in1, in2, in3} = 3'b100;
        #10 {in1, in2, in3} = 3'b101;
        #10 {in1, in2, in3} = 3'b110;
        #10 {in1, in2, in3} = 3'b111;
        #10 {in1, in2, in3} = 3'b000;
        #10 $finish;
    end
endmodule

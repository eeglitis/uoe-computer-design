`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.10.2015 16:46:23
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb();

reg             clk_in;           // 125 MHz clock source
reg             rst_a;            // global async reset wire 
        
// Push-button signals
//
reg    [3:1]    btn_a;            // three push-button wire s
    
// Switch wire  signals
//
reg    [3:0]    switch_a;         // four switch wire s
    
// LED output signals
//
wire   [3:0]    leds;             // four LED outputs
    
// SSD output signals
//
wire   [6:0]    ssd_a;            // PMOD outputs to SSD anodes
wire            ssd_c;            // PMOD output to SSD control
      
// VGA output signals
//
wire   [4:0]    vga_red;          // 5-bit Red signal
wire   [5:0]    vga_green;        // 6-bit Green signal
wire   [4:0]    vga_blue;         // 5- bit Blue signal
wire            vga_hsync;        // Horizontal sync pulse
wire            vga_vsync;        // Vertical sync pulse
    
top dut(
    .clk_in     (clk_in),           // 125 MHz clock source
    .rst_a      (rst_a),            // global async reset input
    .btn_a      (btn_a),            // three push-button inputs
    .switch_a   (switch_a),         // four switch inputs
    .leds       (leds),             // four LED outputs
    .ssd_a      (ssd_a),            // PMOD outputs to SSD anodes
    .ssd_c      (ssd_c),            // PMOD output to SSD control
    .vga_red    (vga_red),          // 5-bit Red signal
    .vga_green  (vga_green),        // 6-bit Green signal
    .vga_blue   (vga_blue),         // 5- bit Blue signal
    .vga_hsync  (vga_hsync),        // Horizontal sync pulse
    .vga_vsync  (vga_vsync)         // Vertical sync pulse
);

initial
begin
     rst_a    = 1'b1;
     btn_a    = 3'd0;
     clk_in   = 1'b0;
     switch_a = 4'd0;
         
    #50 rst_a = 1'b0;
    
    #5000 btn_a[1] = 1'b1;
    #5500 btn_a[1] = 1'b0;
 
    #10000 switch_a = 4'b0001;
    #10050 btn_a[2] = 1'b1;
    #10500 btn_a[2] = 1'b0;

   #17000000 $finish; // slightly more than one video frame
   
end

always
    #4 clk_in = !clk_in;
    
endmodule

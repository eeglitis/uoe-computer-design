`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////
// Company:        The University of Edinburgh
// Engineer:       Nigel Topham
// 
// Create Date:    13:21:43 07/29/2015 
// Design Name:    prac3
// Module Name:    top
// Project Name:   prac3
// Target Devices: Zync-7010
// Tool versions:  Vivado 2015.2
// Description:    Top-level module for practical 3
//
// Dependencies:   
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
////////////////////////////////////////////////////////////////////////

module top(
  input           clk_in,           // 125 MHz external clock source
  input           rst_a,            // global async reset input
    
  // Push-button input signals
  //
  input  [3:1]    btn_a,            // three push-button inputs

  // Switch input signals
  //
  input  [3:0]    switch_a,         // four switch inputs

  // LED output signals
  //
  output [3:0]    leds,             // four LED outputs

  // SSD output signals
  //
  output [6:0]    ssd_a,            // PMOD outputs to SSD anodes
  output          ssd_c,            // PMOD output to SSD control
  
  // VGA output signals
  //
  output [4:0]    vga_red,          // 5-bit Red signal
  output [5:0]    vga_green,        // 6-bit Green signal
  output [4:0]    vga_blue,         // 5- bit Blue signal
  output          vga_hsync,        // Horizontal sync pulse
  output          vga_vsync         // Vertical sync pulse
);

// Define wires for generated system clock, synchronized reset,
// and for input signals that need to be synchronized
// to the system clock.
//
wire            clk;                // 108MHz system clock
wire            reset;              // synchronized reset
wire  [3:1]     buttons;            // synchronized push buttons
wire  [3:0]     switches;           // synchronized switches
wire            locked;             // MMCM PLL lock status

// Define the interface signals for the User Module to use when
// reading and writing to the frame buffer
//
wire  [7:0]     px_col_address;     // select cols 0..255 within FB
wire  [7:0]     px_row_address;     // select rows 0..255 within FB
wire  [15:0]    px_write_data;      // pixel value to be written to FB
wire  [15:0]    px_read_data;       // pixel value read from FB
wire            px_request;         // active-high FB request
wire            px_write;           // 1=> write FB, 0 => read FB
wire            px_ready;           // 1=> write done or read data ready
wire  [7:0]     px_ssd_data;        // user output to SSD (optional use)

// Instantiate the module that produces data for the frame buffer
// (this is the module that you should write)
//
user_logic u_user_logic (
  .clk            (clk            ),
  .reset          (reset          ),
  //
  .buttons        (buttons        ),
  .switches       (switches       ),
  .leds           (leds           ),
  .px_ssd_data    (px_ssd_data    ),
  //
  .px_col_address (px_col_address ),
  .px_row_address (px_row_address ),
  .px_write_data  (px_write_data  ),
  .px_read_data   (px_read_data   ),
  .px_request     (px_request     ),
  .px_write       (px_write       ),
  .px_ready       (px_ready       )
);

// Define the interface signals between the VGA controller and
// the Frame Buffer. The VGA controller only reads from the Frame
// Buffer, in order to refresh the display.
//
wire  [7:0]     vc_col_address;     // select cols 0..255 within FB
wire  [7:0]     vc_row_address;     // select rows 0..255 within FB
wire  [15:0]    vc_read_data;       // pixel value read from FB
wire            vc_request;         // active-high FB request

// Instantiate the VGA controller module
//
vga_control u_vga_control (
  .clk            (clk            ),
  .reset          (reset          ),
  //
  .vc_col_address (vc_col_address ),
  .vc_row_address (vc_row_address ),
  .vc_request     (vc_request     ),
  .vc_read_data   (vc_read_data   ),
  //
  .vga_red        (vga_red        ),
  .vga_green      (vga_green      ),
  .vga_blue       (vga_blue       ),
  .vga_hsync      (vga_hsync      ),
  .vga_vsync      (vga_vsync      )
);

// Instantiate the Frame Buffer module
//
frame_buffer u_frame_buffer (
  .clk            (clk            ),
  .reset          (reset          ),
  //
  .vc_row_address (vc_row_address ),
  .vc_col_address (vc_col_address ),
  .vc_request     (vc_request     ),
  .vc_read_data   (vc_read_data   ),
  //
  .px_row_address (px_row_address ),
  .px_col_address (px_col_address ),
  .px_write_data  (px_write_data  ),
  .px_read_data   (px_read_data   ),
  .px_request     (px_request     ),
  .px_write       (px_write       ),
  .px_ready       (px_ready       )
);

// Instantiate module to drive the SSD from a 4-bit binary integer
//
ssd_driver u_ssd_driver (
  .clk            (clk            ),
  .reset          (reset          ),
  .ssd_input      (px_ssd_data    ),
  .ssd_a          (ssd_a          ),
  .ssd_c          (ssd_c          )
);

// Instantiate module to synchronize asynchronous inputs and provide
// a clean reset signal for the rest of the design
//
resync u_resync (
  .clk            (clk            ),
  .rst_a          (rst_a          ),
  .btn_a          (btn_a          ),
  .switch_a       (switch_a       ),
  .reset          (reset          ),
  .buttons        (buttons        ),
  .switches       (switches       )
);

// Instantiate the clock generator module based on MMCME2_ADV primitive
//
 clk_wiz_0 u_clk_wiz_0 (
  .clk            (clk_in),     // clock input port
  .clk_108MHz     (clk),        // system clock output (108MHz)
  .reset          (rst_a),      // reset signal
  .locked         (locked)      // locked clock generator
 );
 
 endmodule

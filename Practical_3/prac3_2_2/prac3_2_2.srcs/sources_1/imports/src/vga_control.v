`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////
// Company:        The University of Edinburgh
// Engineer:       Nigel Topham
// 
// Create Date:    13:21:43 07/29/2015 
// Design Name:    prac3
// Module Name:    vga_control
// Project Name:   prac3
// Target Devices: Zync-7010
// Tool versions:  Vivado 2015.2
// Description:    VGA controller module for practical 3
//
// Dependencies:   
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
////////////////////////////////////////////////////////////////////////

module vga_control(
  input           clk,              // 108 MHz pixel clock
  input           reset,            // global reset signal
  
  // Frame Buffer read-refresh interface
  //
  output [7:0]    vc_col_address,   // select cols 0..255 within FB
  output [7:0]    vc_row_address,   // select rows 0..255 within FB
  output          vc_request,       // active-high FB request
  input  [15:0]   vc_read_data,     // pixel value read from FB
    
  // VGA output signals
  //
  output [5:1]    vga_red,          // 5-bit Red signal
  output [5:0]    vga_green,        // 6-bit Green signal
  output [5:1]    vga_blue,         // 5- bit Blue signal
  output          vga_hsync,        // Horizontal sync pulse
  output          vga_vsync         // Vertical sync pulse
);

// Define the timing parameters for a 1280x1024 VGA display
//
parameter HLIMIT    = 11'd1687;     // clocks per line
parameter HPULSE    = 11'd112;      // width of hsync pulse
parameter HSTART    = 11'd672;      // delay to display start
parameter HSTOP     = 11'd927;      // delay to display stop
//
parameter VLIMIT    = 11'd1065;     // lines per frame
parameter VPULSE    = 11'd3;        // vsync width, in lines
parameter VSTART    = 11'd426;      // lines of frame to start
parameter VSTOP     = 11'd681;      // lines of frame to end

// Define the registers at stage s1 of the VGA pipeline
//
reg   [10:0]      s1_hcount_r;      // counts horizontal pixel periods
reg   [10:0]      s1_vcount_r;      // counts vertical pixel periods
//
reg   [10:0]      s1_hcount_nxt;    // next s1_hcount_r
reg   [10:0]      s1_vcount_nxt;    // next s1_vcount_r

reg               s1_vcount_en;     // enable vcount clock

// Define the registers at stage s2 of the VGA pipeline
//
reg               s2_hsync_r;       // hsync pulse at s2
reg               s2_vsync_r;       // vsync pulse at s2
reg   [7:0]       s2_col_addr_r;    // column address of next pixel
reg   [7:0]       s2_row_addr_r;    // row address of next pixel
reg               s2_read_en_r;     // read-enable for next pixel
reg               s2_row_enable_r;  // 1=> s2_hcount_r would be in-rnage
reg               s2_col_enable_r;  // 1=> s2_vcount_r would be in-range
//
reg               s2_hsync_nxt;     // next s2_hsync_r
reg               s2_vsync_nxt;     // next s2_vsync_r
reg   [7:0]       s2_col_addr_nxt;  // next s2_col_addr_r
reg   [7:0]       s2_row_addr_nxt;  // next s2_row_addr_r
reg               s2_read_en_nxt;   // next s2_read_en_r
reg               s2_row_enable_nxt;// next s2_row_enable_r
reg               s2_col_enable_nxt;// next s2_col_enable_r

// Define the registers at stage s3 of the VGA pipeline
//
reg   [15:0]      s3_read_data_r;   // pixel read from frame buffer
reg               s3_hsync_r;       // hsync pulse at s3
reg               s3_vsync_r;       // vsync pulse at s3
//
reg   [15:0]      s3_read_data_nxt; // next s3_read_data_r
reg               s3_hsync_nxt;     // next s3_hsync_r
reg               s3_vsync_nxt;     // next s3_vsync_r

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Combinational process defining the next state for all s1 registers //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @*
begin: s1_comb_PROC

  s1_vcount_en      = (s1_hcount_r == 11'd0);
  
  s1_hcount_nxt     = (s1_hcount_r == HLIMIT)
                    ? 11'd0
                    : s1_hcount_r + 1
                    ;
                    
  s1_vcount_nxt     = (s1_vcount_r == VLIMIT) ? 11'd0 : s1_vcount_r + 1;
 
end // s1_comb_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Combinational process defining the next state for all s2 registers //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @*
begin: s2_comb_PROC
  s2_hsync_nxt      = (s1_hcount_r == 11'd0)
                    | (s2_hsync_r & (s1_hcount_r != HPULSE))
                    ;
  
  s2_vsync_nxt      = (s1_vcount_r == 11'd0)
                    | (s2_vsync_r & (s1_vcount_r != VPULSE))
                    ;

  s2_row_enable_nxt = (   (s1_hcount_r >= HSTART)
                        & (s1_hcount_r <= HSTOP) )
                    ;
                    
  s2_col_enable_nxt = (   (s1_vcount_r >= VSTART)
                        & (s1_vcount_r <= VSTOP) )
                    ;
                    
  s2_col_addr_nxt   = s2_col_addr_r + 1;
  
  s2_row_addr_nxt   = (s2_col_addr_r == 8'd255)
                    ? s2_row_addr_r + 1
                    : s2_row_addr_r
                    ;

  s2_read_en_nxt    = (s2_row_enable_nxt & s2_col_enable_nxt);
  
end // s2_comb_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Combinational process defining the next state for all s3 registers //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @*
begin: s3_comb_PROC
  s3_read_data_nxt  = vc_read_data & {16{s2_read_en_r}};
  s3_hsync_nxt      = s2_hsync_r;
  s3_vsync_nxt      = s2_vsync_r;
end // s3_comb_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Sequential process defining all s1 pipeline registers              //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @(posedge clk or posedge reset)
begin: s1_seq_PROC
  if (reset == 1'b1)
    begin
    s1_hcount_r   <= 11'd0;
    s1_vcount_r   <= 11'd0;
    end
  else
    begin
    s1_hcount_r   <= s1_hcount_nxt;
    
    if (s1_vcount_en == 1'b1)
      s1_vcount_r   <= s1_vcount_nxt;
    end
end // s1_seq_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Sequential process defining all s2 pipeline registers              //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @(posedge clk or posedge reset)
begin: s2_seq_PROC
  if (reset == 1'b1)
    begin
    s2_vsync_r      <= 1'b0;
    s2_hsync_r      <= 1'b0;
    s2_col_addr_r   <= 8'd0;
    s2_row_addr_r   <= 8'd0;
    s2_read_en_r    <= 1'b0;
    s2_row_enable_r <= 1'b0;
    s2_col_enable_r <= 1'b0;
    end
  else
    begin
    s2_vsync_r      <= s2_vsync_nxt;
    s2_hsync_r      <= s2_hsync_nxt;
    s2_read_en_r    <= s2_read_en_nxt;
    s2_row_enable_r <= s2_row_enable_nxt;
    s2_col_enable_r <= s2_col_enable_nxt;
    
    if (s2_read_en_r == 1'b1)
      begin
      s2_col_addr_r <= s2_col_addr_nxt;
      s2_row_addr_r <= s2_row_addr_nxt;
      end
    end
end // s2_seq_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Sequential process defining all s3 pipeline registers              //
//                                                                    //
////////////////////////////////////////////////////////////////////////

always @(posedge clk or posedge reset)
begin: s3_seq_PROC
  if (reset == 1'b1)
    begin
    s3_vsync_r      <= 1'b0;
    s3_hsync_r      <= 1'b0;
    s3_read_data_r  <= 16'd0;
    end
  else
    begin
    s3_vsync_r      <= s3_vsync_nxt;
    s3_hsync_r      <= s3_hsync_nxt;
    s3_read_data_r  <= s3_read_data_nxt;
    end
end // s3_seq_PROC

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Assignment of output wires                                         //
//                                                                    //
////////////////////////////////////////////////////////////////////////

assign vc_col_address = s2_col_addr_r;
assign vc_row_address = s2_row_addr_r;
assign vc_request     = s2_read_en_r;
assign vga_red        = s3_read_data_r[15:11];
assign vga_green      = s3_read_data_r[10:5];
assign vga_blue       = s3_read_data_r[4:0];
assign vga_hsync      = s3_hsync_r;
assign vga_vsync      = s3_vsync_r;

endmodule

`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////
// Company:        The University of Edinburgh
// Engineer:       Nigel Topham
// 
// Create Date:    13:21:43 07/29/2015 
// Design Name:    prac3
// Module Name:    user_logic
// Project Name:   prac3
// Target Devices: Zync-7010
// Tool versions:  Vivado 2015.2
// Description:    User logic module for practical 3
//
// Dependencies:   
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
////////////////////////////////////////////////////////////////////////

module user_logic(
  input           clk,              // system clock
  input           reset,            // global reset signal

  // Push-buttons, switches, and LEDs, in case they're needed
  //
  input   [3:1]   buttons,          // three push-button inputs
  input   [3:0]   switches,         // four switch inputs
  output  [3:0]   leds,             // four LED outputs

  // SSD output value, in case the SSD is needed
  //
  output  [7:0]   px_ssd_data,      // output value to SSD driver

  // Read-write interface to the Frame Buffer
  //
  output  [7:0]   px_col_address,   // select cols 0..255 within FB
  output  [7:0]   px_row_address,   // select rows 0..255 within FB
  output  [15:0]  px_write_data,    // pixel value to be written to FB
  output          px_request,       // active-high FB request
  output          px_write,         // 1=> write FB, 0 => read FB
  input   [15:0]  px_read_data,     // pixel value read from FB
  input           px_ready          // 1=> write done or read data ready
);

    localparam S0 = 4'b0000; // button not yet pressed, await press
    localparam S1 = 4'b0001; // button pressed, await release
    localparam S2 = 4'b0010; // button released, part calculation
    localparam S3 = 4'b0011; // remaining calculation
    localparam S4 = 4'b0100; // writing state
    localparam S5 = 4'b0101; // increment state
    localparam S6 = 4'b0110; // select post-processing operation
    localparam S7 = 4'b0111; // subtraction state
    localparam S8 = 4'b1000; // addition state
    localparam S9 = 4'b1001; // swapping state
    localparam S10 = 4'b1010; // writing state for post-processing
    localparam S11 = 4'b1011; // increment state for post-processing  
    
    reg     [15:0]  w;
    reg     [7:0]   x;
    reg     [7:0]   y;
    reg             req_reg;
    reg             write_reg;
    // current state regs
    reg     [3:0]   state;
    reg     [15:0]  write_data_reg;
    reg     [15:0]  result;
    reg     [3:0]   k;
    reg     [15:0]  u;
    reg     [15:0]  v;
    // next state regs
    reg     [3:0]   state_nxt;
    reg     [15:0]  write_data_reg_nxt;
    reg     [15:0]  result_nxt;
    reg     [3:0]   k_nxt;
    reg     [15:0]  u_nxt;
    reg     [15:0]  v_nxt;
    // RGB regs
    reg     [4:0]   r;
    reg     [5:0]   g;
    reg     [4:0]   b;
    reg     [4:0]   r_new;
    reg     [5:0]   g_new;
    reg     [4:0]   b_new;
    
    // combinational logic
    
    always @*
    begin
        init;
        case (state)
            S0:         begin
                        if (buttons[1] == 1'b1) state_nxt = S1;
                        else if (buttons[2] == 1'b1) state_nxt = S6;
                        end
            // states for part 2
            S1:         if (buttons[1] == 1'b0) state_nxt = S2;
            S2:         calc_1;
            S3:         calc_2;
            S4:         write;
            S5:         increment;
            // states for part 3 - post-processing
            S6:         op_select;
            S7:         sub;
            S8:         add;
            S9:         swap;
            S10:        pp_write;
            S11:        pp_increment;
            default:    state_nxt = S0;
        endcase
    end
    
    task init;
    begin
        // for all states
        req_reg = 1'b0;
        write_reg = 1'b0;
        state_nxt = state;
        // changed in S2 / S5
        u_nxt = u;
        v_nxt = v;
        // changed in S3 / S5 / S7 / S8 / S9
        result_nxt = result;
        // changed in S5 / S11
        write_data_reg_nxt = write_data_reg;
        // changed only in S5
        k_nxt = k;
    end
    endtask

    // S2: calc_1 task - calculate u and v
    
    task calc_1;
    begin   
        {x, y} = write_data_reg;
        u_nxt = (x-8'd128)*(x-8'd128);
        v_nxt = (y-8'd128)*(y-8'd128);
        state_nxt = S3;
    end
    endtask
    
    // S3: calc_2 task - calculate next result
    
    task calc_2;
    begin
        w = (u+v)*(u+v);
        result_nxt = ((w<<k) | (w>>5'd16-k));
        state_nxt = S4;
    end
    endtask
    
    // S4: write task - keep values stable until ready is asserted
    
    task write;
    begin
        req_reg = 1'b1;
        write_reg = 1'b1;
        if (px_ready == 1'b1) state_nxt = S5;
    end
    endtask
    
    // S5: increment task - either increment, or reset if finished and increment k
    
    task increment;
    begin
        if (write_data_reg == 16'd65535)
        begin
            state_nxt = S0;
            {u_nxt, v_nxt} = 32'b0;
            result_nxt = 16'b0;
            write_data_reg_nxt = 16'b0;
            k_nxt = k + 4'b1;
        end
        else
        begin
            write_data_reg_nxt = write_data_reg + 16'b1;
            state_nxt = S2;
        end
    end
    endtask
    
    // S6: select operation based on switch status. also read the current pixel value
    
    task op_select;
    begin
        req_reg = 1'b1;
        if (buttons[2] == 1'b0)
        begin
            if (switches == 4'b0001) state_nxt = S7;        //subtraction
            else if (switches == 4'b0010) state_nxt = S8;   //addition
            else if (switches == 4'b0011) state_nxt = S9;   //switch
            else state_nxt = S0;                            //invalid switch combination
        end
    end
    endtask
    
    // S7: execute subtraction operation, change result
    
    task sub;
    begin
        {r, g, b} = px_read_data;
        if (r>0) r_new = r-1'b1;
        else r_new = 5'b0;
        if (g>0) g_new = g-1'b1;
        else g_new = 6'b0;
        if (b>0) b_new = b-1'b1;
        else b_new = 5'b0;
        result_nxt = {r_new, g_new, b_new};
        state_nxt = S10;
    end
    endtask
    
    // S8: execute addition operation, change result
    
    task add;
    begin
        {r, g, b} = px_read_data;
        if (r<5'd31) r_new = r+1'b1;
        else r_new = 5'd31;
        if (g<6'd63) g_new = g+1'b1;
        else g_new = 6'd63;
        if (b<5'd31) b_new = b+1'b1;
        else b_new = 5'd31;
        result_nxt = {r_new, g_new, b_new};
        state_nxt = S10;
    end
    endtask
      
    // S9: swap R and B values
    
    task swap;
    begin
        {r, g, b} = px_read_data;
        result_nxt = {b, g, r};
        state_nxt = S10;
    end
    endtask
    
    // S10: write task for post-processing
    
    task pp_write;
    begin
        req_reg = 1'b1;
        write_reg = 1'b1;
        if (px_ready == 1'b1) state_nxt = S11;
    end
    endtask
    
    // S11: increment task for post-processing
    
    task pp_increment;
    begin
        if (write_data_reg == 16'd65535)
        begin
            state_nxt = S0;
            result_nxt = 16'b0;
            write_data_reg_nxt = 16'b0;
        end
        else
        begin
            write_data_reg_nxt = write_data_reg + 16'b1;
            state_nxt = S6;
        end
    end
    endtask
    
    // sequential logic
    
    always @(posedge clk or posedge reset)
    begin
        if (reset == 1'b1)
        begin
            state <= S0;
            {u, v} <= 32'b0;
            result <= 16'b0;
            write_data_reg <= 16'b0;
            k <= 4'b0;
        end
        else
        begin
            state <= state_nxt;
            u <= u_nxt;
            v <= v_nxt;
            result <= result_nxt;
            write_data_reg <= write_data_reg_nxt;
            k <= k_nxt;
        end
    end
    
    // assign output values
    
    assign leds             = k;
    assign px_write_data    = result;
    assign px_row_address   = write_data_reg[15:8];
    assign px_col_address   = write_data_reg[7:0];
    assign px_request       = req_reg;
    assign px_write         = write_reg;
    
    assign px_ssd_data    = 8'd0;                   // default assignment
    
endmodule

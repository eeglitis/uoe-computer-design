`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////
// Company:        The University of Edinburgh
// Engineer:       Nigel Topham
// 
// Create Date:    13:21:43 07/29/2015 
// Design Name:    prac3
// Module Name:    user_logic
// Project Name:   prac3
// Target Devices: Zync-7010
// Tool versions:  Vivado 2015.2
// Description:    User logic module for practical 3
//
// Dependencies:   
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
////////////////////////////////////////////////////////////////////////

module user_logic(
  input           clk,              // system clock
  input           reset,            // global reset signal

  // Push-buttons, switches, and LEDs, in case they're needed
  //
  input   [3:1]   buttons,          // three push-button inputs
  input   [3:0]   switches,         // four switch inputs
  output  [3:0]   leds,             // four LED outputs

  // SSD output value, in case the SSD is needed
  //
  output  [7:0]   px_ssd_data,      // output value to SSD driver

  // Read-write interface to the Frame Buffer
  //
  output  [7:0]   px_col_address,   // select cols 0..255 within FB
  output  [7:0]   px_row_address,   // select rows 0..255 within FB
  output  [15:0]  px_write_data,    // pixel value to be written to FB
  output          px_request,       // active-high FB request
  output          px_write,         // 1=> write FB, 0 => read FB
  input   [15:0]  px_read_data,     // pixel value read from FB
  input           px_ready          // 1=> write done or read data ready
);

    localparam S0 = 4'b0001; // button not yet pressed, await press
    localparam S1 = 4'b0010; // button pressed, await release
    localparam S2 = 4'b0100; // button released, write to display
    
    // current state regs
    reg     [3:0]   state;
    reg     [15:0]  write_data_reg;
    reg             req_reg;
    reg             write_reg;
    // next state regs
    reg     [3:0]   state_nxt;
    reg     [15:0]  write_data_reg_nxt;
    
    // combinational logic
    
    always @*
    begin
        init;
        case (state)
            S0:         if (buttons[1] == 1'b1) state_nxt = S1;
            S1:         if (buttons[1]== 1'b0) state_nxt = S2;
            S2:         write;
            default:    state_nxt = S0;
        endcase
    end
    
    task init;
    begin
        state_nxt = state;
        write_data_reg_nxt = write_data_reg;
        req_reg = 1'b0;
        write_reg = 1'b0;
    end
    endtask

    // writing task - increment next state values
    // only accessed when state=S2
    
    task write;
    begin
        req_reg = 1'b1;
        write_reg = 1'b1;    
        if (px_ready == 1'b1)
	    begin
            if (write_data_reg == 16'd65535)
                state_nxt = S0;
            else
                write_data_reg_nxt = write_data_reg + 16'b1;
	    end    
    end
    endtask
    
    // sequential logic
    
    always @(posedge clk or posedge reset)
    begin
        if (reset == 1'b1)
        begin
            state <= S0;
            write_data_reg <= 16'b0;
        end
        else
        begin
            state <= state_nxt;
            write_data_reg <= write_data_reg_nxt;
        end
    end
    
    // assign output values
    
    assign leds             = state;
    assign px_write_data    = write_data_reg;
    assign px_row_address   = write_data_reg[15:8];
    assign px_col_address   = write_data_reg[7:0];
    assign px_request       = req_reg;
    assign px_write         = write_reg;
    
    assign px_ssd_data    = 8'd0;                   // default assignment
    
endmodule

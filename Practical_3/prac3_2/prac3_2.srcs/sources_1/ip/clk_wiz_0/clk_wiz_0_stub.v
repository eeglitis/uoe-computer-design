// Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2015.2 (lin64) Build 1266856 Fri Jun 26 16:35:25 MDT 2015
// Date        : Wed Nov  4 15:35:44 2015
// Host        : rothley.inf.ed.ac.uk running 64-bit Scientific Linux release 7.1 (Nitrogen)
// Command     : write_verilog -force -mode synth_stub
//               /afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_1/prac3_1.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0_stub.v
// Design      : clk_wiz_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-3
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_wiz_0(clk, clk_108MHz, reset, locked)
/* synthesis syn_black_box black_box_pad_pin="clk,clk_108MHz,reset,locked" */;
  input clk;
  output clk_108MHz;
  input reset;
  output locked;
endmodule

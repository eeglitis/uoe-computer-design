`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.10.2015 14:54:33
// Design Name: 
// Module Name: px_bus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module px_bus(
    input           clk,
    input           reset,
    input           button,           // push-button 1 to initiate operation
    input   [15:0]  px_read_data,     // pixel value read from FB
    input           px_ready,         // 1=> write done or read data ready
    output  [3:0]   leds,
    output  [7:0]   px_col_address,   // select cols 0..255 within FB
    output  [7:0]   px_row_address,   // select rows 0..255 within FB
    output  [15:0]  px_write_data,    // pixel value to be written to FB
    output          px_request,       // active-high FB request
    output          px_write          // 1=> write FB, 0 => read FB
    );
    
    localparam S0 = 4'b0001; // button not yet pressed, await press
    localparam S1 = 4'b0010; // button pressed, await release
    localparam S2 = 4'b0100; // button released, write to display
    
    reg     [15:0]  u;
    reg     [15:0]  v;
    reg     [15:0]  w;
    // current state regs
    reg     [3:0]   state;
    reg     [15:0]  write_data_reg;
    reg     [15:0]  result;
    reg     [3:0]   k;
    reg     [7:0]   x;
    reg     [7:0]   y;
    reg             req_reg;
    reg             write_reg;
    // next state regs
    reg     [3:0]   state_nxt;
    reg     [15:0]  write_data_reg_nxt;
    reg     [15:0]  result_nxt;
    reg     [3:0]   k_nxt;
    
    // combinational logic
    
    always @*
    begin
        init;
        case (state)
            S0:         if (button == 1'b1) state_nxt = S1;
            S1:         if (button == 1'b0) state_nxt = S2;
            S2:         begin
                        write;
                        rotate;
                        end
            default:    state_nxt = S0;
        endcase
    end
    
    task init;
    begin
        state_nxt = state;
        write_data_reg_nxt = write_data_reg;
        //{x, y} = write_data_reg;
        result_nxt = result;
        k_nxt = k;
        req_reg = 1'b0;
        write_reg = 1'b0;
    end
    endtask

    // writing task - increment next state values
    // only accessed when state=S2
    
    task write;
    begin
        req_reg = 1'b1;
        write_reg = 1'b1;    
        if (px_ready == 1'b1)
	    begin
            if (write_data_reg == 16'd65535)
            begin
                state_nxt = S0;
                write_data_reg_nxt = 16'b0;
                result_nxt = 16'b0;
                k_nxt = k + 4'b1;
            end
            else
                write_data_reg_nxt = write_data_reg + 16'b1;
	    end
    end
    endtask
    
    // rotation task
    
    task rotate;
    begin
        {x, y} = write_data_reg_nxt;
        u = (x-8'd128)*(x-8'd128);
        v = (y-8'd128)*(y-8'd128);
        w = (u+v)*(u+v);
        result_nxt = ((w<<k) | (w>>5'd16-k));
    end
    endtask
    
    // sequential logic
    
    always @(posedge clk or posedge reset)
    begin
        if (reset == 1'b1)
        begin
            state <= S0;
            write_data_reg <= 16'b0;
            result <= 16'b0;
            k <= 4'b0;
        end
        else
        begin
            state <= state_nxt;
            write_data_reg <= write_data_reg_nxt;
            result <= result_nxt;
            k <= k_nxt;
        end
    end
    
    // assign output values
    
    assign leds             = k;
    assign px_write_data    = result;
    assign px_row_address   = write_data_reg[15:8];
    assign px_col_address   = write_data_reg[7:0];
    assign px_request       = req_reg;
    assign px_write         = write_reg;

endmodule

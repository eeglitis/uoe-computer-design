`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////
// Company:        The University of Edinburgh
// Engineer:       Nigel Topham
// 
// Create Date:    13:21:43 07/29/2015 
// Design Name:    prac3
// Module Name:    user_logic
// Project Name:   prac3
// Target Devices: Zync-7010
// Tool versions:  Vivado 2015.2
// Description:    User logic module for practical 3
//
// Dependencies:   
//
// Revision: 
// Revision 1.0 - File Created
// Additional Comments: 
//
////////////////////////////////////////////////////////////////////////

module user_logic(
  input           clk,              // system clock
  input           reset,            // global reset signal

  // Push-buttons, switches, and LEDs, in case they're needed
  //
  input   [3:1]   buttons,          // three push-button inputs
  input   [3:0]   switches,         // four switch inputs
  output  [3:0]   leds,             // four LED outputs

  // SSD output value, in case the SSD is needed
  //
  output  [7:0]   px_ssd_data,      // output value to SSD driver

  // Read-write interface to the Frame Buffer
  //
  output  [7:0]   px_col_address,   // select cols 0..255 within FB
  output  [7:0]   px_row_address,   // select rows 0..255 within FB
  output  [15:0]  px_write_data,    // pixel value to be written to FB
  output          px_request,       // active-high FB request
  output          px_write,         // 1=> write FB, 0 => read FB
  input   [15:0]  px_read_data,     // pixel value read from FB
  input           px_ready          // 1=> write done or read data ready
);

////////////////////////////////////////////////////////////////////////
//                                                                    //
// Assignment of output wires                                         //
//                                                                    //
////////////////////////////////////////////////////////////////////////

//assign leds           = {buttons, reset};       // default assignment
assign px_ssd_data    = 8'd0;                   // default assignment
//assign px_col_address = 8'd0;                   // default assignment
//assign px_row_address = 8'd0;                   // default assignment
//assign px_write_data  = 16'd0;                  // default assignment
//assign px_request     = 1'b0;                   // default assignment
//assign px_write       = 1'b0;                   // default assignment

px_bus u_px_bus (
    .clk            (clk),
    .reset          (reset),
    .button         (buttons[1]),
    .px_read_data   (px_read_data),
    .px_ready       (px_ready),
    .leds           (leds),
    .px_col_address (px_col_address),
    .px_row_address (px_row_address),
    .px_write_data  (px_write_data),
    .px_request     (px_request),
    .px_write       (px_write)
);

endmodule

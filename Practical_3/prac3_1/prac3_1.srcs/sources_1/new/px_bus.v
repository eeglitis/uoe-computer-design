`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.10.2015 14:54:33
// Design Name: 
// Module Name: px_bus
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module px_bus(
    input           clk,
    input           reset,
    input           button,           // push-button 1 to initiate operation
    input   [15:0]  px_read_data,     // pixel value read from FB
    input           px_ready,         // 1=> write done or read data ready
    output  [3:0]   leds,
    output  [7:0]   px_col_address,   // select cols 0..255 within FB
    output  [7:0]   px_row_address,   // select rows 0..255 within FB
    output  [15:0]  px_write_data,    // pixel value to be written to FB
    output          px_request,       // active-high FB request
    output          px_write          // 1=> write FB, 0 => read FB
    );
    
    localparam S0 = 4'b0001; // button not yet pressed, await press
    localparam S1 = 4'b0010; // button pressed, await release
    localparam S2 = 4'b0100; // button released, write to display
    
    // current state regs
    reg     [3:0]   state;
    reg     [15:0]  write_data_reg;
    reg             req_reg;
    reg             write_reg;
    // next state regs
    reg     [3:0]   state_nxt;
    reg     [15:0]  write_data_reg_nxt;
    
    // combinational logic
    
    always @*
    begin
        init;
        case (state)
            S0:         if (button == 1'b1) state_nxt = S1;
            S1:         if (button == 1'b0) state_nxt = S2;
            S2:         write;
            default:    state_nxt = S0;
        endcase
    end
    
    task init;
    begin
        state_nxt = state;
        write_data_reg_nxt = write_data_reg;
        req_reg = 1'b0;
        write_reg = 1'b0;
    end
    endtask

    // writing task - increment next state values
    // only accessed when state=S2
    
    task write;
    begin
        req_reg = 1'b1;
        write_reg = 1'b1;    
        if (px_ready == 1'b1)
	    begin
            if (write_data_reg == 16'd65535)
                state_nxt = S0;
            else
                write_data_reg_nxt = write_data_reg + 16'b1;
	    end    
    end
    endtask
    
    // sequential logic
    
    always @(posedge clk or posedge reset)
    begin
        if (reset == 1'b1)
        begin
            state <= S0;
            write_data_reg <= 16'b0;
        end
        else
        begin
            state <= state_nxt;
            write_data_reg <= write_data_reg_nxt;
        end
    end
    
    // assign output values
    
    assign leds             = state;
    assign px_write_data    = write_data_reg;
    assign px_row_address   = write_data_reg[15:8];
    assign px_col_address   = write_data_reg[7:0];
    assign px_request       = req_reg;
    assign px_write         = write_reg;

endmodule

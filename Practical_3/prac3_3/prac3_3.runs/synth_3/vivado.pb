
�
Command: %s
53*	vivadotcl2r
^synth_design -top top -part xc7z010clg400-3 -flatten_hierarchy none -keep_equivalent_registers2default:defaultZ4-113h px
7
Starting synth_design
149*	vivadotclZ4-321h px
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7z0102default:defaultZ17-347h px
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7z0102default:defaultZ17-349h px
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:05 ; elapsed = 00:00:05 . Memory (MB): peak = 1063.770 ; gain = 154.520 ; free physical = 1114 ; free virtual = 13488
2default:defaulth px
�
synthesizing module '%s'638*oasys2
top2default:default2{
e/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/top.v2default:default2
222default:default8@Z8-638h px
�
synthesizing module '%s'638*oasys2

user_logic2default:default2�
l/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/user_logic.v2default:default2
222default:default8@Z8-638h px
J
%s*synth25
!	Parameter S0 bound to: 4'b0000 
2default:defaulth px
J
%s*synth25
!	Parameter S1 bound to: 4'b0001 
2default:defaulth px
J
%s*synth25
!	Parameter S2 bound to: 4'b0010 
2default:defaulth px
J
%s*synth25
!	Parameter S3 bound to: 4'b0011 
2default:defaulth px
J
%s*synth25
!	Parameter S4 bound to: 4'b0100 
2default:defaulth px
J
%s*synth25
!	Parameter S5 bound to: 4'b0101 
2default:defaulth px
J
%s*synth25
!	Parameter S6 bound to: 4'b0110 
2default:defaulth px
J
%s*synth25
!	Parameter S7 bound to: 4'b0111 
2default:defaulth px
J
%s*synth25
!	Parameter S8 bound to: 4'b1000 
2default:defaulth px
J
%s*synth25
!	Parameter S9 bound to: 4'b1001 
2default:defaulth px
K
%s*synth26
"	Parameter S10 bound to: 4'b1010 
2default:defaulth px
K
%s*synth26
"	Parameter S11 bound to: 4'b1011 
2default:defaulth px
�
%done synthesizing module '%s' (%s#%s)256*oasys2

user_logic2default:default2
12default:default2
12default:default2�
l/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/user_logic.v2default:default2
222default:default8@Z8-256h px
�
synthesizing module '%s'638*oasys2
vga_control2default:default2�
m/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/vga_control.v2default:default2
222default:default8@Z8-638h px
V
%s*synth2A
-	Parameter HLIMIT bound to: 11'b11010010111 
2default:defaulth px
V
%s*synth2A
-	Parameter HPULSE bound to: 11'b00001110000 
2default:defaulth px
V
%s*synth2A
-	Parameter HSTART bound to: 11'b01010100000 
2default:defaulth px
U
%s*synth2@
,	Parameter HSTOP bound to: 11'b01110011111 
2default:defaulth px
V
%s*synth2A
-	Parameter VLIMIT bound to: 11'b10000101001 
2default:defaulth px
V
%s*synth2A
-	Parameter VPULSE bound to: 11'b00000000011 
2default:defaulth px
V
%s*synth2A
-	Parameter VSTART bound to: 11'b00110101010 
2default:defaulth px
U
%s*synth2@
,	Parameter VSTOP bound to: 11'b01010101001 
2default:defaulth px
�
%done synthesizing module '%s' (%s#%s)256*oasys2
vga_control2default:default2
22default:default2
12default:default2�
m/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/vga_control.v2default:default2
222default:default8@Z8-256h px
�
synthesizing module '%s'638*oasys2 
frame_buffer2default:default2�
n/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/frame_buffer.v2default:default2
212default:default8@Z8-638h px
\
%s*synth2G
3	Parameter RAM_WIDTH bound to: 16 - type: integer 
2default:defaulth px
Z
%s*synth2E
1	Parameter RAM_MSB bound to: 15 - type: integer 
2default:defaulth px
`
%s*synth2K
7	Parameter RAM_ADDR_BITS bound to: 16 - type: integer 
2default:defaulth px
b
%s*synth2M
9	Parameter RAM_MAX_WORD bound to: 65536 - type: integer 
2default:defaulth px
M
%s*synth28
$	Parameter PX_IDLE bound to: 2'b00 
2default:defaulth px
P
%s*synth2;
'	Parameter PX_RD_WAIT bound to: 2'b01 
2default:defaulth px
Q
%s*synth2<
(	Parameter PX_RD_READY bound to: 2'b10 
2default:defaulth px
�
%done synthesizing module '%s' (%s#%s)256*oasys2 
frame_buffer2default:default2
32default:default2
12default:default2�
n/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/frame_buffer.v2default:default2
212default:default8@Z8-256h px
�
synthesizing module '%s'638*oasys2

ssd_driver2default:default2�
l/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/ssd_driver.v2default:default2
232default:default8@Z8-638h px
P
%s*synth2;
'	Parameter BLANK bound to: 7'b0000000 
2default:defaulth px
O
%s*synth2:
&	Parameter ZERO bound to: 7'b0111111 
2default:defaulth px
N
%s*synth29
%	Parameter ONE bound to: 7'b0000110 
2default:defaulth px
N
%s*synth29
%	Parameter TWO bound to: 7'b1011011 
2default:defaulth px
P
%s*synth2;
'	Parameter THREE bound to: 7'b1001111 
2default:defaulth px
O
%s*synth2:
&	Parameter FOUR bound to: 7'b1100110 
2default:defaulth px
O
%s*synth2:
&	Parameter FIVE bound to: 7'b1101101 
2default:defaulth px
N
%s*synth29
%	Parameter SIX bound to: 7'b1111101 
2default:defaulth px
P
%s*synth2;
'	Parameter SEVEN bound to: 7'b0000111 
2default:defaulth px
P
%s*synth2;
'	Parameter EIGHT bound to: 7'b1111111 
2default:defaulth px
O
%s*synth2:
&	Parameter NINE bound to: 7'b1101111 
2default:defaulth px
O
%s*synth2:
&	Parameter DASH bound to: 7'b1000000 
2default:defaulth px
�
%done synthesizing module '%s' (%s#%s)256*oasys2

ssd_driver2default:default2
42default:default2
12default:default2�
l/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/ssd_driver.v2default:default2
232default:default8@Z8-256h px
�
synthesizing module '%s'638*oasys2
resync2default:default2~
h/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/resync.v2default:default2
232default:default8@Z8-638h px
�
%done synthesizing module '%s' (%s#%s)256*oasys2
resync2default:default2
52default:default2
12default:default2~
h/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/resync.v2default:default2
232default:default8@Z8-256h px
�
synthesizing module '%s'638*oasys2
	clk_wiz_02default:default2�
�/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.runs/synth_3/.Xil/Vivado-28649-glanton.inf.ed.ac.uk/realtime/clk_wiz_0_stub.v2default:default2
62default:default8@Z8-638h px
�
%done synthesizing module '%s' (%s#%s)256*oasys2
	clk_wiz_02default:default2
62default:default2
12default:default2�
�/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.runs/synth_3/.Xil/Vivado-28649-glanton.inf.ed.ac.uk/realtime/clk_wiz_0_stub.v2default:default2
62default:default8@Z8-256h px
�
%done synthesizing module '%s' (%s#%s)256*oasys2
top2default:default2
72default:default2
12default:default2{
e/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/sources_1/imports/src/top.v2default:default2
222default:default8@Z8-256h px
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:09 ; elapsed = 00:00:09 . Memory (MB): peak = 1234.207 ; gain = 324.957 ; free physical = 944 ; free virtual = 13315
2default:defaulth px
A
%s*synth2,

Report Check Netlist: 
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
r
%s*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
r
%s*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:09 ; elapsed = 00:00:10 . Memory (MB): peak = 1234.207 ; gain = 324.957 ; free physical = 944 ; free virtual = 13315
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
S
Loading part %s157*device2#
xc7z010clg400-32default:defaultZ21-403h px
H
)Preparing netlist for logic optimization
349*projectZ1-570h px
;

Processing XDC Constraints
244*projectZ1-262h px
:
Initializing timing engine
348*projectZ1-569h px
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.runs/synth_3/.Xil/Vivado-28649-glanton.inf.ed.ac.uk/dcp/clk_wiz_0_in_context.xdc2default:default2
u_clk_wiz_02default:defaultZ20-848h px
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.runs/synth_3/.Xil/Vivado-28649-glanton.inf.ed.ac.uk/dcp/clk_wiz_0_in_context.xdc2default:default2
u_clk_wiz_02default:defaultZ20-847h px
�
Parsing XDC File [%s]
179*designutils2v
b/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/constrs_1/new/timing.xdc2default:defaultZ20-179h px
�
Finished Parsing XDC File [%s]
178*designutils2v
b/afs/inf.ed.ac.uk/user/s13/s1353184/Desktop/CD/prac3/prac3_3/prac3_3.srcs/constrs_1/new/timing.xdc2default:defaultZ20-178h px
E
&Completed Processing XDC Constraints

245*projectZ1-263h px
{
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:00.012default:default2
00:00:00.012default:default2
1414.2152default:default2
0.0002default:default2
8942default:default2
132642default:defaultZ17-722h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:16 ; elapsed = 00:00:17 . Memory (MB): peak = 1414.219 ; gain = 504.969 ; free physical = 891 ; free virtual = 13261
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
S
%s*synth2>
*Start Loading Part and Timing Information
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
G
%s*synth22
Loading part: xc7z010clg400-3
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:16 ; elapsed = 00:00:17 . Memory (MB): peak = 1414.219 ; gain = 504.969 ; free physical = 891 ; free virtual = 13261
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
W
%s*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:16 ; elapsed = 00:00:17 . Memory (MB): peak = 1414.219 ; gain = 504.969 ; free physical = 891 ; free virtual = 13261
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	state_nxt2default:defaultZ8-5546h px
r
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
v_nxt2default:defaultZ8-5546h px
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	state_nxt2default:defaultZ8-5546h px
{
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2"
s2_row_addr_r02default:defaultZ8-5546h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:16 ; elapsed = 00:00:18 . Memory (MB): peak = 1414.219 ; gain = 504.969 ; free physical = 869 ; free virtual = 13239
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
I
%s*synth24
 Start RTL Component Statistics 
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
7
%s*synth2"
+---Adders : 
2default:defaulth px
W
%s*synth2B
.	   2 Input     16 Bit       Adders := 4     
2default:defaulth px
W
%s*synth2B
.	   2 Input     11 Bit       Adders := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      8 Bit       Adders := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      6 Bit       Adders := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      5 Bit       Adders := 5     
2default:defaulth px
W
%s*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth px
:
%s*synth2%
+---Registers : 
2default:defaulth px
W
%s*synth2B
.	               16 Bit    Registers := 6     
2default:defaulth px
W
%s*synth2B
.	               11 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                8 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                4 Bit    Registers := 4     
2default:defaulth px
W
%s*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                2 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth px
5
%s*synth2 
+---RAMs : 
2default:defaulth px
W
%s*synth2B
.	            1024K Bit         RAMs := 1     
2default:defaulth px
6
%s*synth2!
+---Muxes : 
2default:defaulth px
W
%s*synth2B
.	  13 Input     16 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input     16 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      6 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth px
W
%s*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	  13 Input      3 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   5 Input      1 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	  13 Input      1 Bit        Muxes := 6     
2default:defaulth px
W
%s*synth2B
.	   4 Input      1 Bit        Muxes := 2     
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
L
%s*synth27
#Finished RTL Component Statistics 
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
V
%s*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
L
%s*synth27
#Hierarchical RTL Component report 
2default:defaulth px
<
%s*synth2'
Module user_logic 
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
7
%s*synth2"
+---Adders : 
2default:defaulth px
W
%s*synth2B
.	   2 Input     16 Bit       Adders := 4     
2default:defaulth px
W
%s*synth2B
.	   2 Input      6 Bit       Adders := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      5 Bit       Adders := 5     
2default:defaulth px
W
%s*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth px
:
%s*synth2%
+---Registers : 
2default:defaulth px
W
%s*synth2B
.	               16 Bit    Registers := 4     
2default:defaulth px
W
%s*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth px
6
%s*synth2!
+---Muxes : 
2default:defaulth px
W
%s*synth2B
.	  13 Input     16 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      6 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      5 Bit        Muxes := 4     
2default:defaulth px
W
%s*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	  13 Input      3 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   5 Input      1 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	  13 Input      1 Bit        Muxes := 6     
2default:defaulth px
=
%s*synth2(
Module vga_control 
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
7
%s*synth2"
+---Adders : 
2default:defaulth px
W
%s*synth2B
.	   2 Input     11 Bit       Adders := 2     
2default:defaulth px
W
%s*synth2B
.	   2 Input      8 Bit       Adders := 2     
2default:defaulth px
:
%s*synth2%
+---Registers : 
2default:defaulth px
W
%s*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth px
W
%s*synth2B
.	               11 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                8 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth px
>
%s*synth2)
Module frame_buffer 
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
:
%s*synth2%
+---Registers : 
2default:defaulth px
W
%s*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth px
W
%s*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth px
5
%s*synth2 
+---RAMs : 
2default:defaulth px
W
%s*synth2B
.	            1024K Bit         RAMs := 1     
2default:defaulth px
6
%s*synth2!
+---Muxes : 
2default:defaulth px
W
%s*synth2B
.	   2 Input     16 Bit        Muxes := 2     
2default:defaulth px
W
%s*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth px
W
%s*synth2B
.	   4 Input      1 Bit        Muxes := 2     
2default:defaulth px
<
%s*synth2'
Module ssd_driver 
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
6
%s*synth2!
+---Muxes : 
2default:defaulth px
W
%s*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth px
8
%s*synth2#
Module resync 
2default:defaulth px
H
%s*synth23
Detailed RTL Component Info : 
2default:defaulth px
:
%s*synth2%
+---Registers : 
2default:defaulth px
W
%s*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth px
W
%s*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
X
%s*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
E
%s*synth20
Start Part Resource Summary
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px

%s*synth2j
VPart Resources:
DSPs: 80 (col length:40)
BRAMs: 120 (col length: RAMB18 40 RAMB36 20)
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
H
%s*synth23
Finished Part Resource Summary
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Start Parallel Synthesis Optimization  : Time (s): cpu = 00:00:16 ; elapsed = 00:00:18 . Memory (MB): peak = 1414.219 ; gain = 504.969 ; free physical = 869 ; free virtual = 13239
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
K
%s*synth26
"Start Cross Boundary Optimization
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	state_nxt2default:defaultZ8-5546h px
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	state_nxt2default:defaultZ8-5546h px
r
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
v_nxt2default:defaultZ8-5546h px
d
%s*synth2O
;DSP Report: Generating DSP v_nxt0, operation Mode is: A*B.
2default:defaulth px
b
%s*synth2M
9DSP Report: operator v_nxt0 is absorbed into DSP v_nxt0.
2default:defaulth px
d
%s*synth2O
;DSP Report: Generating DSP u_nxt0, operation Mode is: A*B.
2default:defaulth px
b
%s*synth2M
9DSP Report: operator u_nxt0 is absorbed into DSP u_nxt0.
2default:defaulth px
i
%s*synth2T
@DSP Report: Generating DSP result_nxt2, operation Mode is: A*B.
2default:defaulth px
l
%s*synth2W
CDSP Report: operator result_nxt2 is absorbed into DSP result_nxt2.
2default:defaulth px
{
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2"
s2_row_addr_r02default:defaultZ8-5546h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Cross Boundary Optimization : Time (s): cpu = 00:00:17 ; elapsed = 00:00:19 . Memory (MB): peak = 1414.223 ; gain = 504.973 ; free physical = 853 ; free virtual = 13215
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Parallel Reinference  : Time (s): cpu = 00:00:17 ; elapsed = 00:00:19 . Memory (MB): peak = 1414.223 ; gain = 504.973 ; free physical = 853 ; free virtual = 13215
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
/
%s*synth2

ROM:
2default:defaulth px
f
%s*synth2Q
=+------------+------------+---------------+----------------+
2default:defaulth px
g
%s*synth2R
>|Module Name | RTL Object | Depth x Width | Implemented As | 
2default:defaulth px
f
%s*synth2Q
=+------------+------------+---------------+----------------+
2default:defaulth px
g
%s*synth2R
>|ssd_driver  | rom        | 256x14        | LUT            | 
2default:defaulth px
g
%s*synth2R
>|ssd_driver  | rom        | 256x14        | LUT            | 
2default:defaulth px
g
%s*synth2R
>+------------+------------+---------------+----------------+

2default:defaulth px
5
%s*synth2 

Block RAM:
2default:defaulth px
�
%s*synth2�
�+------------+---------------+------------------------+---+---+------------------------+---+---+---------+--------+--------+----------------------------+
2default:defaulth px
�
%s*synth2�
�|Module Name | RTL Object    | PORT A (Depth x Width) | W | R | PORT B (Depth x Width) | W | R | OUT_REG | RAMB18 | RAMB36 | Hierarchical Name          | 
2default:defaulth px
�
%s*synth2�
�+------------+---------------+------------------------+---+---+------------------------+---+---+---------+--------+--------+----------------------------+
2default:defaulth px
�
%s*synth2�
�|top         | fb_memory_reg | 64 K x 16(READ_FIRST)  | W | R |                        |   |   | Port A  | 0      | 32     | top/frame_buffer/extram__2 | 
2default:defaulth px
�
%s*synth2�
�+------------+---------------+------------------------+---+---+------------------------+---+---+---------+--------+--------+----------------------------+

2default:defaulth px
�
%s*synth2�
�Note: The table shows the Block RAMs at the current stage of the synthesis flow. Some Block RAMs may be reimplemented as non Block RAM primitives later in the synthesis flow. Multiple instantiated Block RAMs are reported only once. "Hierarchical Name" reflects the Block RAM name as it appears in the hierarchical module and only part of it is displayed.
DSP:
2default:defaulth px
�
%s*synth2�
�+------------+-------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px
�
%s*synth2�
�|Module Name | DSP Mapping | Neg Edge Clk | A Size | B Size | C Size | D Size | P Size | AREG | BREG | CREG | DREG | ADREG | MREG | PREG | 
2default:defaulth px
�
%s*synth2�
�+------------+-------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px
�
%s*synth2�
�|user_logic  | A*B         | No           | 16     | 16     | 48     | 25     | 32     | 0    | 0    | 1    | 1    | 1     | 0    | 0    | 
2default:defaulth px
�
%s*synth2�
�|user_logic  | A*B         | No           | 16     | 16     | 48     | 25     | 32     | 0    | 0    | 1    | 1    | 1     | 0    | 0    | 
2default:defaulth px
�
%s*synth2�
�|user_logic  | A*B         | No           | 16     | 16     | 48     | 25     | 32     | 0    | 0    | 1    | 1    | 1     | 0    | 0    | 
2default:defaulth px
�
%s*synth2�
�+------------+-------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+

2default:defaulth px
�
%s*synth2�
�Note: The table shows the DSPs inferred at the current stage of the synthesis flow. Some DSP may be reimplemented as non DSP primitives later in the synthesis flow. Multiple instantiated DSPs are reported only once.
2default:defaulth px
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
\sync_btn_1_r_reg[3] 2default:default2
resync2default:defaultZ8-3332h px
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2)
\sync_btn_2_r_reg[3] 2default:default2
resync2default:defaultZ8-3332h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
A
%s*synth2,
Start Area Optimization
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Area Optimization : Time (s): cpu = 00:00:18 ; elapsed = 00:00:20 . Memory (MB): peak = 1414.223 ; gain = 504.973 ; free physical = 852 ; free virtual = 13214
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Parallel Area Optimization  : Time (s): cpu = 00:00:18 ; elapsed = 00:00:20 . Memory (MB): peak = 1414.223 ; gain = 504.973 ; free physical = 852 ; free virtual = 13214
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
�
%s*synth2�
�Finished Parallel Synthesis Optimization  : Time (s): cpu = 00:00:18 ; elapsed = 00:00:20 . Memory (MB): peak = 1414.223 ; gain = 504.973 ; free physical = 852 ; free virtual = 13214
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
C
%s*synth2.
Start Timing Optimization
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
O
%s*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2l
XINFO: Moved 1 constraints on hierarchical pins to their respective driving/loading pins
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:29 ; elapsed = 00:00:30 . Memory (MB): peak = 1440.215 ; gain = 530.965 ; free physical = 772 ; free virtual = 13135
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:00:29 ; elapsed = 00:00:31 . Memory (MB): peak = 1459.215 ; gain = 549.965 ; free physical = 752 ; free virtual = 13115
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
B
%s*synth2-
Start Technology Mapping
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_02default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_12default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_22default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_32default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_42default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_52default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_62default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_72default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_82default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2%
fb_memory_reg_1_92default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_102default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_112default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_122default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_132default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_142default:defaultZ8-4480h px
�
�The timing for the instance %s (implemented as a block RAM) might be sub-optimal as no optional output register could be merged into the block ram. Providing additional output register may help in improving timing.
3630*oasys2&
fb_memory_reg_1_152default:defaultZ8-4480h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:00:30 ; elapsed = 00:00:32 . Memory (MB): peak = 1475.230 ; gain = 565.980 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
<
%s*synth2'
Start IO Insertion
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
E
%s*synth20
Start Final Netlist Cleanup
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
H
%s*synth23
Finished Final Netlist Cleanup
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_user_logic2default:default2

buttons[3]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[7]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[6]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[5]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[4]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[3]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[2]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[1]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2 
u_ssd_driver2default:default2 
ssd_input[0]2default:defaultZ8-3295h px
�
'tying undriven pin %s:%s to constant 0
3295*oasys2
u_resync2default:default2
btn_a[3]2default:defaultZ8-3295h px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:00:30 ; elapsed = 00:00:32 . Memory (MB): peak = 1475.230 ; gain = 565.980 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
A
%s*synth2,

Report Check Netlist: 
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
r
%s*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
r
%s*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth px
r
%s*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
L
%s*synth27
#Start Renaming Generated Instances
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:30 ; elapsed = 00:00:32 . Memory (MB): peak = 1475.230 ; gain = 565.980 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
B
%s*synth2-

Report RTL Partitions: 
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
T
%s*synth2?
++-+--------------+------------+----------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
H
%s*synth23
Start Writing Synthesis Report
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
>
%s*synth2)

Report BlackBoxes: 
2default:defaulth px
L
%s*synth27
#+------+--------------+----------+
2default:defaulth px
L
%s*synth27
#|      |BlackBox name |Instances |
2default:defaulth px
L
%s*synth27
#+------+--------------+----------+
2default:defaulth px
L
%s*synth27
#|1     |clk_wiz_0     |         1|
2default:defaulth px
L
%s*synth27
#+------+--------------+----------+
2default:defaulth px
>
%s*synth2)

Report Cell Usage: 
2default:defaulth px
E
%s*synth20
+------+-----------+------+
2default:defaulth px
E
%s*synth20
|      |Cell       |Count |
2default:defaulth px
E
%s*synth20
+------+-----------+------+
2default:defaulth px
E
%s*synth20
|1     |clk_wiz_0  |     1|
2default:defaulth px
E
%s*synth20
|2     |CARRY4     |    14|
2default:defaulth px
E
%s*synth20
|3     |DSP48E1    |     3|
2default:defaulth px
E
%s*synth20
|4     |LUT1       |    46|
2default:defaulth px
E
%s*synth20
|5     |LUT2       |    76|
2default:defaulth px
E
%s*synth20
|6     |LUT3       |    50|
2default:defaulth px
E
%s*synth20
|7     |LUT4       |    59|
2default:defaulth px
E
%s*synth20
|8     |LUT5       |    43|
2default:defaulth px
E
%s*synth20
|9     |LUT6       |    74|
2default:defaulth px
E
%s*synth20
|10    |MUXF7      |    18|
2default:defaulth px
E
%s*synth20
|11    |RAMB36E1   |    11|
2default:defaulth px
E
%s*synth20
|12    |RAMB36E1_1 |    11|
2default:defaulth px
E
%s*synth20
|13    |RAMB36E1_2 |     5|
2default:defaulth px
E
%s*synth20
|14    |RAMB36E1_3 |     5|
2default:defaulth px
E
%s*synth20
|15    |FDCE       |   168|
2default:defaulth px
E
%s*synth20
|16    |IBUF       |     7|
2default:defaulth px
E
%s*synth20
|17    |OBUF       |    30|
2default:defaulth px
E
%s*synth20
+------+-----------+------+
2default:defaulth px
B
%s*synth2-

Report Instance Areas: 
2default:defaulth px
Y
%s*synth2D
0+------+-----------------+-------------+------+
2default:defaulth px
Y
%s*synth2D
0|      |Instance         |Module       |Cells |
2default:defaulth px
Y
%s*synth2D
0+------+-----------------+-------------+------+
2default:defaulth px
Y
%s*synth2D
0|1     |top              |             |   622|
2default:defaulth px
Y
%s*synth2D
0|2     |  u_user_logic   |user_logic   |   307|
2default:defaulth px
Y
%s*synth2D
0|3     |  u_vga_control  |vga_control  |   139|
2default:defaulth px
Y
%s*synth2D
0|4     |  u_frame_buffer |frame_buffer |    73|
2default:defaulth px
Y
%s*synth2D
0|5     |  u_ssd_driver   |ssd_driver   |    49|
2default:defaulth px
Y
%s*synth2D
0|6     |  u_resync       |resync       |    15|
2default:defaulth px
Y
%s*synth2D
0+------+-----------------+-------------+------+
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:30 ; elapsed = 00:00:32 . Memory (MB): peak = 1475.230 ; gain = 565.980 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
{
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px
p
%s*synth2[
GSynthesis finished with 0 errors, 0 critical warnings and 12 warnings.
2default:defaulth px
�
%s*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:24 ; elapsed = 00:00:25 . Memory (MB): peak = 1475.230 ; gain = 269.445 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
�
%s*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:31 ; elapsed = 00:00:32 . Memory (MB): peak = 1475.230 ; gain = 565.980 ; free physical = 738 ; free virtual = 13101
2default:defaulth px
?
 Translating synthesized netlist
350*projectZ1-571h px
c
-Analyzing %s Unisim elements for replacement
17*netlist2
562default:defaultZ29-17h px
g
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px
H
)Preparing netlist for logic optimization
349*projectZ1-570h px
r
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px
{
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px
R
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
492default:default2
122default:default2
02default:default2
02default:defaultZ4-41h px
[
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:00:292default:default2
00:00:312default:default2
1475.2342default:default2
457.4652default:default2
7372default:default2
131002default:defaultZ17-722h px
�
�report_utilization: Time (s): cpu = 00:00:00.02 ; elapsed = 00:00:00.05 . Memory (MB): peak = 1507.250 ; gain = 0.000 ; free physical = 733 ; free virtual = 13096
*commonh px
}
Exiting %s at %s...
206*common2
Vivado2default:default2,
Sun Nov 15 14:42:59 20152default:defaultZ17-206h px
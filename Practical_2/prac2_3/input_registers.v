`timescale 1ns / 1ps
///////////////////////////////////////////////////////////////////////
// Company:         The University of Edinburgh
// Engineer:        Nigel Topham
// 
// Create Date:     17.09.2015 12:31:35
// Design Name:     Practical 2
// Module Name:     input_registers
// Project Name:    Computer Design
// Target Devices:  Zync-7010
// Tool Versions:   2015.2
// Description:     Module to capture input values from switches
// 
// Dependencies:    none
// 
// Revision:
// Revision 1.0 -   File Created
// Additional Comments:
//  This module implements two 3-bit registers, in1 and in2, and
//  allows the enclosing module to load those registers by asserting
//  the load_in1 or load_in2 inputs while presenting the value to load
//  on the 3-bit switches input.
//
///////////////////////////////////////////////////////////////////////


module input_registers(
    input             clk,      // clock input
    input             reset,    // reset input
    input             load_in1, // load-enable for in1 register
    input             load_in2, // load-enable for in2 register
    input      [2:0]  switches, // switch input values (async)
    output reg [2:0]  in1,      // in1 register output
    output reg [2:0]  in2       // in2 register output
);
    
always @(posedge clk or posedge reset)
begin: reg_PROC
    if (reset == 1'b1)
        begin
        in1 <= 3'd0;
        in2 <= 3'd0;
        end
    else
        begin
        if (load_in1 == 1'b1)
            in1 <= switches;
        if (load_in2 == 1'b1)
            in2 <= switches;
        end
end

endmodule

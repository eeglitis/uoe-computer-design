`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.10.2015 14:29:29
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder(
    input   [5:0]   in1,
    input   [5:0]   in2,
    output  [7:0]   result
    );
    
wire            carry_0_1;
wire            carry_1_2;
wire            carry_2_3;
wire            carry_3_4;
wire            carry_4_5;
wire            carry_5_6;
        
            fulladder u_fulladder_0 (
                .A      (in1[0]),
                .B      (in2[0]),
                .Cin    (1'b0),
                .S      (result[0]),
                .Cout   (carry_0_1)
            );
            
            fulladder u_fulladder_1 (
                .A      (in1[1]),
                .B      (in2[1]),
                .Cin    (carry_0_1),
                .S      (result[1]),
                .Cout   (carry_1_2)
            );
            
            fulladder u_fulladder_2 (
                .A      (in1[2]),
                .B      (in2[2]),
                .Cin    (carry_1_2),
                .S      (result[2]),
                .Cout   (carry_2_3)
            );
            
            fulladder u_fulladder_3 (
                .A      (in1[3]),
                .B      (in2[3]),
                .Cin    (carry_2_3),
                .S      (result[3]),
                .Cout   (carry_3_4)
            );
            
            fulladder u_fulladder_4 (
                .A      (in1[4]),
                .B      (in2[4]),
                .Cin    (carry_3_4),
                .S      (result[4]),
                .Cout   (carry_4_5)
            );
            
            fulladder u_fulladder_5 (
                .A      (in1[5]),
                .B      (in2[5]),
                .Cin    (carry_4_5),
                .S      (result[5]),
                .Cout   (carry_5_6)
            );
                                             
            assign result[6] = carry_5_6;
            assign result[7] = 1'b0;
        
endmodule

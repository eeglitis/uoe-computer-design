`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.10.2015 15:19:43
// Design Name: 
// Module Name: mainadder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mainadder(
    input   [2:0]   in1,
    input   [2:0]   in2,
    output  [7:0]   result
    );
    
    reg     [5:0]   res1;
    reg     [5:0]   res2;
    reg     [5:0]   res3;
    wire    [7:0]   mid;
    
    always @(*)
    begin
        res1[0] = in1[0] & in2[0];
        res1[1] = in1[1] & in2[0];
        res1[2] = in1[2] & in2[0];
        res1[3] = 1'b0;
        res1[4] = 1'b0;
        res1[5] = 1'b0;
        res2[0] = 1'b0;
        res2[1] = in1[0] & in2[1];
        res2[2] = in1[1] & in2[1];
        res2[3] = in1[2] & in2[1];
        res2[4] = 1'b0;
        res2[5] = 1'b0;
        res3[0] = 1'b0;
        res3[1] = 1'b0;
        res3[2] = in1[0] & in2[2];
        res3[3] = in1[1] & in2[2];
        res3[4] = in1[2] & in2[2];
        res3[5] = 1'b0;
    end
    
    adder u_adder_0 (
        .in1    (res1),
        .in2    (res2),
        .result (mid)
    );
    
    adder u_adder_1 (
        .in1    (mid),
        .in2    (res3),
        .result (result)
    ); 
endmodule

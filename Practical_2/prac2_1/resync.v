`timescale 1ns / 1ps
///////////////////////////////////////////////////////////////////////
// Company:         The University of Edinburgh
// Engineer:        Nigel Topham
// 
// Create Date:     17.09.2015 14:48:41
// Design Name:     Practical 2
// Module Name:     resync
// Project Name:    Computer Design
// Target Devices:  Zync-7010
// Tool Versions:   2015.2
// Description:     Module to resynchronize external inputs
// 
// Dependencies:    none
// 
// Revision:
// Revision 1.0  - File Created
// Additional Comments:
//  This module resynchronizes a collection of asynchronous external
//  input signals to the local clock (clk) and provides those
//  resynchronized signals as outputs.
//  The module also implements reset synchronization in order to 
//  produce a reset output from the rst_a and clk inputs. The reset
//  signal is then asynchronously asserted, and synchronously
//  rescinded.
//  The top.xdc file should contain 'false path' constraints from
//  each asynchronous input pin to all flip-flops.
// 
///////////////////////////////////////////////////////////////////////


module resync(
    input       clk,            // clock input
    input       rst_a,          // async reset input
    input       load_in1_a,     // async button 1 in
    input       load_in2_a,     // async button 2 in
    output      reset,          // sync reset out
    output      load_in1,       // sync button 1 out
    output      load_in2        // sync button 2 out
);

// Declare reg variables through which to infer pairs of synchronizing
// flip-flops. Note, it is good practice to give all synchronizing
// flip-flops a standard prefix, e.g. "sync_" so they can be easily
// identified in constraint scripts after synthesis, if needed.

reg [1:0] sync_resetn_r;
reg [1:0] sync_load_in1_r;
reg [1:0] sync_load_in2_r;

always @(posedge clk or posedge rst_a)
begin: resync_PROC
    if (rst_a == 1'b1)
        begin
        sync_resetn_r   <= 2'b00;
        sync_load_in1_r <= 2'b00;
        sync_load_in2_r <= 2'b00;
        end
    else
        begin
        sync_resetn_r   <= {sync_resetn_r[0],   1'b1};
        sync_load_in1_r <= {sync_load_in1_r[0], load_in1_a};
        sync_load_in2_r <= {sync_load_in2_r[0], load_in2_a};
        end
end // resync_PROC

// Assign outputs from the second synchronizing flip-flop

assign load_in1 =  sync_load_in1_r[1];
assign load_in2 =  sync_load_in2_r[1];
assign reset    = !sync_resetn_r[1];   // reset is active high

endmodule

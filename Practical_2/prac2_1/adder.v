`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.10.2015 14:29:29
// Design Name: 
// Module Name: adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder(
    input   [2:0]   in1,
    input   [2:0]   in2,
    output  [7:0]   result
    );
    
wire    carry_0_1;
wire    carry_1_2;
wire    carry_2_3;

    fulladder u_fulladder_0 (
        .A      (in1[0]),
        .B      (in2[0]),
        .Cin    (1'b0),
        .S      (result[0]),
        .Cout   (carry_0_1)
    );
    
    fulladder u_fulladder_1 (
        .A      (in1[1]),
        .B      (in2[1]),
        .Cin    (carry_0_1),
        .S      (result[1]),
        .Cout   (carry_1_2)
    );
    
    fulladder u_fulladder_2 (
        .A      (in1[2]),
        .B      (in2[2]),
        .Cin    (carry_1_2),
        .S      (result[2]),
        .Cout   (result[3])
    );
    
    assign result[4] = 1'b0;
    assign result[5] = 1'b0;
    assign result[6] = 1'b0;
    assign result[7] = 1'b0;

endmodule

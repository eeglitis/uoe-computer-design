`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.10.2015 14:54:35
// Design Name: 
// Module Name: fsm_b
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_b(
    input clock,
    input reset,
    input step,
    input [2:0] switches,
    output reg s0,
    output reg s1,
    output reg s2,
    output reg s3
    );
    
    parameter T0 = 2'b00;
    parameter T1 = 2'b01;
    parameter T2 = 2'b10;
    parameter T3 = 2'b11;
    
    reg enable;
    reg [1:0] s;
    reg [1:0] s_nxt;
    
    always @(step)
    begin
        enable = step;
    end
    
    always @(posedge clock or posedge reset)
    begin
        if (reset == 1'b1)
            s <= T0;
        else
            s <= s_nxt;
    end
    
    always @(*)
    begin
        s_nxt = s;
        if (enable == 1'b1)
        begin
            case(s)
                T0: begin
                        s0 = 1'b1;
                        s1 = 1'b0;
                        s2 = 1'b0;
                        s3 = 1'b0;
                        if (switches[0] == 0)
                            s_nxt = T3;
                        else
                            s_nxt = T1;
                    end
                T1: begin
                        s1 = 1'b1;
                        s0 = 1'b0;
                        s2 = 1'b0;
                        s3 = 1'b0;
                        if (switches[1] == 0)
                            s_nxt = T0;
                        else
                            s_nxt = T2;
                    end
                T2: begin
                        s2 = 1'b1;
                        s0 = 1'b0;
                        s1 = 1'b0;
                        s3 = 1'b0;
                        if (switches[2] == 0)
                            s_nxt = T0;
                        else
                            s_nxt = T3;
                    end
                T3: begin
                        s3 = 1'b1;
                        s0 = 1'b0;
                        s1 = 1'b0;
                        s2 = 1'b0;
                        s_nxt = T0;
                    end
            endcase
        end
    end
    
endmodule

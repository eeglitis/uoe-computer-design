`timescale 1ns / 1ps
///////////////////////////////////////////////////////////////////////
// Company:         The University of Edinburgh
// Engineer:        <put your name here>, Nigel Topham
// 
// Create Date:     17.09.2015 12:09:16
// Design Name:     Practical 2
// Module Name:     top
// Project Name:    Computer Design
// Target Devices:  Zync-7010
// Tool Versions:   2015.2
// Description:     Top-level module for practical 2
// 
// Dependencies:    top.xdc, input_registers.v ssd_driver.v resync.v
// 
// Revision:
// Revision 1.0 -   File Created
// Additional Comments:
//
//   Keep the overall structure as it is defined already, and insert 
//   your logic from line 112. You have two 3-bit inputs (in1 and in2) 
//   available, and you can output an 8-bit unsigned binary-coded
//   integer on the seven-segment display by setting the value of the
//   result vector.
//
//   Set the three right-most switches on the ZYBO to the input value
//   you want to capture, and then press push button 1 to load in1,
//   or press button 2 to load in1. Press push button 0 to reset the
//   system.
// 
///////////////////////////////////////////////////////////////////////


module top(
    input        clk,           // 125 MHz clock input
    input        rst_a,         // push button 2
    input [3:0]  switches,      // slider switches 0 to 3
    input        load_in1_a,    // push button 0
    input        load_in2_a,    // push button 1
    output [3:0] led           // the four LEDs
    //output [6:0] ssd_a,         // PMOD outputs to SSD anodes
    //output       ssd_c          // PMOD output to SSD control
    );
    
// Declare wires for the synchronized versions of rst_a, load_in1_a 
// and load_in2_a.

wire            reset;
wire            load_in1;
wire            load_in2;
wire            step;

// Declare wires to accept input register values from the input 
// register module.

wire    [2:0]   in1;
wire    [2:0]   in2;

// Use the result[7:0] reg as the value to be displayed on the
// Seven-Segment-Display.

//wire    [7:0]   result;

// Instantiate module to synchronize the asynchronous external inputs
// from the three push buttons. The reason for having this module 
// will become apparent in lecture 9 (Asynchronous Sequential Logic)...

resync u_resync (
    .clk        (clk),
    .rst_a      (rst_a),
    .load_in1_a (load_in1_a),
    .load_in2_a (load_in2_a),
    .reset      (reset),
    .load_in1   (load_in1),
    .load_in2   (load_in2)
);

// Instantiate module to capture and store two 3-bit operands from
// the switches.

input_registers u_input_registers (
    .clk        (clk),
    .reset      (reset),
    .load_in1   (load_in1),
    .load_in2   (load_in2),
    .switches   (switches[2:0]),
    .in1        (in1),
    .in2        (in2)
);

// Instantiate module to drive the SSD from an 8-bit signed binary 
// value. The SSD is able to display values in the range [-9..+99]. 
// Values outside this range are displayed as two dashes --.

/*ssd_driver u_ssd_driver (
    .clk        (clk),      // clock input
    .reset      (reset),    // reset input
    .ssd_input  (result),   // value to display, 8-bit integer
    .ssd_a      (ssd_a),    // 7-bit unary code to drive display
    .ssd_c      (ssd_c)     // control signal to switch between digits
);*/

// Assign default values to the LED outputs
// You can replace this with your own code if you wish, using the LEDs 
// in any way you want...

//assign led = {switches[3], load_in2, load_in1, reset};

//=====================================================================
// Remove line 112 (which is just for test purposes) and insert your 
// own code...

//assign result = {1'b0, in1} * {1'b0, in2};

//3-bit adder implementation

fsm_a u_fsm_a (
    .clock    (clk),
    .reset    (reset),
    .switch   (switches[3]),
    .step     (step)
);

fsm_b u_fsm_b (
    .clock    (clk),
    .reset    (reset),
    .step     (step),
    .switches (switches[2:0]),
    .s0       (led[0]),
    .s1       (led[1]),
    .s2       (led[2]),
    .s3       (led[3]) 
);

//=====================================================================

endmodule

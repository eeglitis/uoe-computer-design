`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.10.2015 14:17:36
// Design Name: 
// Module Name: dflipflop
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dflipflop(
    input           clock,
    input           reset,
    input       [22:0] D,
    output reg  [22:0] Q
    );

    always @(posedge clock or posedge reset)
    begin
        if (reset == 1'b1)
            Q <= 8'd00000000;
        else
            Q <= D;
    end
endmodule

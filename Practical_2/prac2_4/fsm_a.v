`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.10.2015 14:17:36
// Design Name: 
// Module Name: fsm_a
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_a(
    input clock,
    input reset,
    input switch,
    output reg step
    );
    
    reg             pps;
    reg     [25:0]   counter;
    reg     [25:0]   counter_nxt;
    
    always @(switch)
    begin
        pps = switch;
    end
    
    always @(*)
    begin
        if (((pps == 1'b0) && (counter_nxt > 26'd31250000)) || ((pps == 1'b1) && (counter_nxt > 26'd62500000)))
        begin // switch flipped during cycle
            counter = 26'b0;
            step = 1'b0;
        end
        else if (((pps == 1'b0) && (counter_nxt == 26'd31250000)) || ((pps == 1'b1) && (counter_nxt == 26'd62500000)))
        begin
            step = 1'b1;
            counter = 26'b0;
        end
        else //not reset, not reached required # of cycles - just increment
        begin
            counter = counter_nxt + 26'd1;
            step = 1'b0;
        end
    end
    
    always @(posedge clock or posedge reset)
    begin
        if (reset == 1'b1)
            counter_nxt <= 8'd00000000;
        else
            counter_nxt <= counter;
    end
    
endmodule

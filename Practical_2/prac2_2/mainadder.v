`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.10.2015 15:19:43
// Design Name: 
// Module Name: mainadder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mainadder(
    input   [2:0]   in1,
    input   [2:0]   in2,
    input           switch,
    output  [7:0]   result
    );
    
    wire    [2:0]   inv2;
    reg     [2:0]   in2f;
    reg             cin;   
    assign inv2 = ~in2;
    
    always @(*)
    begin
    if (switch == 1'b1)
        begin
        in2f = inv2;
        cin = 1'b1;
        end
    else
        begin
        in2f = in2;
        cin = 1'b0;
        end
    end
    
    adder u_adder (
        .in1    (in1),
        .in2    (in2f),
        .cin    (cin),
        .result (result)
    );    
endmodule
